package controller;

import model.Model;

public class Controller {
    private Model model;

    public Controller(Model model) {
        this.model = model;
    }

    public void setInputFileName(String filename) throws RuntimeException {
        model.setInputFileName(filename);
    }

    public void setOutputFileName(String filename) throws RuntimeException {
        model.setOutputFileName(filename);
    }

    public void writeError(String message) throws RuntimeException {
        model.writeError(message);
    }

    public void makeSearchQueries() {
        model.makeSearchQueries();
    }

    public void makeStatQuery() {
        model.makeStatQuery();
    }
}
