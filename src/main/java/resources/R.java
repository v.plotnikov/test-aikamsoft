package resources;

public final class R {
    public static final String FIRSTNAME = "firstName";
    public static final String LASTNAME = "lastName";
    public static final String PRODUCT_NAME = "productName";
    public static final String MIN_TIMES = "minTimes";
    public static final String MIN_EXPENSES = "minExpenses";
    public static final String MAX_EXPENSES = "maxExpenses";
    public static final String BAD_CUSTOMERS = "badCustomers";
}
