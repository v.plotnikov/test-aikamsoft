package view;

import controller.Controller;

public class View {
    public enum Operation {
        SEARCH, STAT
    }

    private Controller controller;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void start(String[] args) {
        try {
            controller.setOutputFileName(args[2]);

            try {
                controller.setInputFileName(args[1]);

                switch (parseOperation(args[0])) {
                    case SEARCH:
                        controller.makeSearchQueries();
                        break;
                    case STAT:
                        controller.makeStatQuery();
                        break;
                }
            } catch (RuntimeException e) {
                controller.writeError(e.getMessage());
            }
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }

    private Operation parseOperation(String s) throws IllegalArgumentException {
        Operation operation = null;
        switch (s) {
            case "search":
                operation = Operation.SEARCH;
                break;
            case "stat":
                operation = Operation.STAT;
                break;
            default:
                throw new IllegalArgumentException("Неподдерживаемая операция");
        }
        return operation;
    }
}
