import controller.Controller;
import model.Model;
import view.View;

public class Application {
    public static void main(String[] args) {
        try {
            Model model = new Model();
            Controller controller = new Controller(model);
            View view = new View(controller);

            view.start(args);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }
    }
}
