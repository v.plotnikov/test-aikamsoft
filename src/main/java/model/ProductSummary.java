package model;

public class ProductSummary {
    private Product product;
    private int expenses;

    public ProductSummary(Product product, int expenses) {
        this.product = product;
        this.expenses = expenses;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getExpenses() {
        return expenses;
    }

    public void setExpenses(int expenses) {
        this.expenses = expenses;
    }
}
