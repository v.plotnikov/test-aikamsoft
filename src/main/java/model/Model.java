package model;

import com.google.gson.*;
import model.file.FileReaderImpl;
import model.file.FileWriterImpl;
import resources.R;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Model {
    private Repository repository;
    private FileReaderImpl fileReaderImpl = new FileReaderImpl();
    private FileWriterImpl fileWriterImpl = new FileWriterImpl();

    public Model() {
        try {
            repository = new Repository();
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void setInputFileName(String filename) throws RuntimeException {
        fileReaderImpl.setFilename(filename);
        fileReaderImpl.read();
    }

    public void setOutputFileName(String filename) throws RuntimeException {
        fileWriterImpl.setFilename(filename);
    }

    public void writeError(String message) throws RuntimeException {
        fileWriterImpl.writeError(message);
    }

    public void makeSearchQueries() {
        ArrayList<JsonObject> criteria = fileReaderImpl.getCriteria();

        JsonArray array = new JsonArray();
        try {
            for (JsonObject c : criteria) {
                if (c.has(R.LASTNAME)) {
                    array.add(makeSearchJson(c, repository.getCustomersByLastName(c.get(R.LASTNAME).getAsString())));
                } else if (c.has(R.PRODUCT_NAME) && c.has(R.MIN_TIMES)) {
                    array.add(makeSearchJson(c, repository.getCustomersByProductNameAndPurchasedCount(c.get(R.PRODUCT_NAME).getAsString(), c.get(R.MIN_TIMES).getAsInt())));
                } else if (c.has(R.MIN_EXPENSES) && c.has(R.MAX_EXPENSES)) {
                    array.add(makeSearchJson(c, repository.getCustomersWithAverageExpensesBetween(c.get(R.MIN_EXPENSES).getAsInt(), c.get(R.MAX_EXPENSES).getAsInt())));
                } else if (c.has(R.BAD_CUSTOMERS)) {
                    array.add(makeSearchJson(c, repository.getBadCustomers(c.get(R.BAD_CUSTOMERS).getAsInt())));
                } else {
                    throw new RuntimeException(String.format("Неизвестный критерий: %s", c));
                }
            }

            fileWriterImpl.writeSearch(array);
        } catch (RuntimeException e) {
            fileWriterImpl.writeError(e.getMessage());
        }
    }

    private JsonObject makeSearchJson(JsonObject c, ArrayList<Customer> customers) {
        JsonObject criteriaObject = new JsonObject();
        criteriaObject.add("criteria", c);

        JsonArray customersArray = new JsonArray();
        for (Customer customer : customers) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(R.LASTNAME, customer.getLastName());
            jsonObject.addProperty(R.FIRSTNAME, customer.getFirstName());
            customersArray.add(jsonObject);
        }

        criteriaObject.add("results", customersArray);
        return criteriaObject;
    }

    public void makeStatQuery() {
        JsonObject dates = fileReaderImpl.getDates();

        try {
            if (dates == null) {
                throw new RuntimeException("Ошибка при получении данных из файла");
            }

            String startDate = dates.get("startDate").getAsString();
            String endDate = dates.get("endDate").getAsString();
            ArrayList<Pair<Customer, ArrayList<ProductSummary>>> data = repository.getStatsByPeriod(startDate, endDate);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            long totalDays = TimeUnit.DAYS.convert(dateFormat.parse(endDate).getTime() - dateFormat.parse(startDate).getTime(), TimeUnit.MILLISECONDS);
            if (totalDays < 0) {
                throw new RuntimeException("Некорректный формат даты");
            }
            long totalExpenses = 0;

            JsonArray customers = new JsonArray();
            for (Pair<Customer, ArrayList<ProductSummary>> el : data) {
                JsonObject c = new JsonObject();
                c.addProperty("name", el.getKey().getLastName() + " " + el.getKey().getFirstName());

                JsonArray purchases = new JsonArray();
                long customerExpenses = 0;
                for (ProductSummary product : el.getValue()) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("expenses", product.getExpenses());
                    customerExpenses += product.getExpenses();
                    jsonObject.addProperty("name", product.getProduct().getName());
                    purchases.add(jsonObject);
                }
                c.add("purchases", purchases);
                c.addProperty("totalExpenses", customerExpenses);
                totalExpenses += customerExpenses;

                customers.add(c);
            }

            double avgExpenses = 0;
            if (customers.size() > 0) {
                avgExpenses = (double) totalExpenses / customers.size();
            }

            fileWriterImpl.writeStat(totalDays, totalExpenses, avgExpenses, customers);
        } catch (RuntimeException | ParseException e) {
            fileWriterImpl.writeError(e.getMessage());
        }
    }
}
