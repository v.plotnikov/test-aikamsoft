package model.file;

import com.google.gson.JsonArray;

public interface BaseFileWriter {
    void setFilename(String filename) throws RuntimeException;

    void writeSearch(JsonArray array) throws RuntimeException;

    void writeStat(long totalDays, long totalExpenses, double avgExpenses, JsonArray array) throws RuntimeException;

    void writeError(String message) throws RuntimeException;
}
