package model.file;

import com.google.gson.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

public class FileWriterImpl implements BaseFileWriter {
    private FileOutputStream outputStream = null;
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public FileWriterImpl() {
    }

    @Override
    public void setFilename(String filename) throws RuntimeException {
        try {
            outputStream = new FileOutputStream(filename);
        } catch (IOException e) {
            throw new RuntimeException("Не удалось открыть файл для записи");
        }
    }

    @Override
    public void writeSearch(JsonArray array) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", "search");
        jsonObject.add("results", array);
        write(jsonObject);
    }

    @Override
    public void writeStat(long totalDays, long totalExpenses, double avgExpenses, JsonArray array) {
        JsonObject output = new JsonObject();
        output.addProperty("type", "stat");
        output.addProperty("totalDays", totalDays);
        output.add("customers", array);
        output.addProperty("totalExpenses", totalExpenses);
        output.addProperty("avgExpenses", String.format(Locale.ENGLISH, "%.2f", avgExpenses));
        write(output);
    }

    @Override
    public void writeError(String message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", "error");
        jsonObject.addProperty("message", message);
        write(jsonObject);
    }

    private void write(JsonObject s) {
        try {
            outputStream.write(gson.toJson(JsonParser.parseString(s.toString())).getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            System.out.println("Не удалось записать данные в файл");
        }
    }
}
