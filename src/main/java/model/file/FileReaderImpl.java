package model.file;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class FileReaderImpl implements BaseFileReader {
    private FileInputStream inputStream = null;

    private JsonObject data = null;

    public FileReaderImpl() {
    }

    @Override
    public void setFilename(String filename) throws RuntimeException {
        try {
            inputStream = new FileInputStream(filename);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Файл с входными данными не найден");
        }
    }

    @Override
    public void read() throws RuntimeException {
        if (inputStream != null) {
            try {
                byte[] bytes = new byte[inputStream.available()];
                if (inputStream.read(bytes) == 0) throw new IOException();

                data = JsonParser.parseString(new String(bytes, StandardCharsets.UTF_8)).getAsJsonObject();
            } catch (IOException e) {
                throw new RuntimeException("Не удалось прочесть входные данные");
            }
        }
    }

    @Override
    public ArrayList<JsonObject> getCriteria() {
        ArrayList<JsonObject> criteria = new ArrayList<>();
        if (data != null) {
            JsonArray array = data.getAsJsonArray("criteria");
            for (int i = 0; i < array.size(); ++i) {
                criteria.add(array.get(i).getAsJsonObject());
            }
        }
        return criteria;
    }

    @Override
    public JsonObject getDates() {
        return data;
    }
}
