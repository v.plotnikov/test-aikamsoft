package model.file;

import com.google.gson.JsonObject;

import java.util.ArrayList;

public interface BaseFileReader {
    void setFilename(String filename) throws RuntimeException;

    void read() throws RuntimeException;

    ArrayList<JsonObject> getCriteria();

    JsonObject getDates();
}
