package model;

import java.sql.*;
import java.util.ArrayList;

public class Repository {
    private final String URL = "jdbc:postgresql://127.0.0.1:5432/aikamsoft";
    private final String USERNAME = "postgres";
    private final String PASSWORD = "admin";

    private Connection connection = null;

    public Repository() throws SQLException {
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public Customer getCustomerById(int id) {
        String sql = String.format("SELECT * FROM customers WHERE id = %d", id);
        Customer customer = null;

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);
            resultSet.next();

            customer = new Customer(resultSet.getInt("id"), resultSet.getString("firstname"), resultSet.getString("lastname"));
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при получении покупателя по идентификатору");
        }

        return customer;
    }

    public Product getProductById(int id) throws RuntimeException {
        String sql = String.format("SELECT * FROM products WHERE id = %d", id);
        Product product = null;

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);
            resultSet.next();

            product = new Product(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("price"));
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при получении продукта по идентификатору");
        }

        return product;
    }

    public ArrayList<Customer> getCustomersByLastName(String lastName) throws RuntimeException {
        String sql = String.format("SELECT * FROM customers WHERE lastname = '%s'", lastName);
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            while (resultSet.next()) {
                customers.add(new Customer(resultSet.getInt("id"), resultSet.getString("firstname"), resultSet.getString("lastname")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при получении покупателей по фамилии");
        }

        return customers;
    }

    public ArrayList<Customer> getCustomersByProductNameAndPurchasedCount(String productName, int boughtTimes) throws RuntimeException {
        String sql = String.format("WITH \"query\" as (\n" +
                "SELECT u.customerid as customer_id, r.\"name\" as product_name, count(u.customerid) as purchased_count\n" +
                "FROM purchases u JOIN products r ON u.productid = r.id GROUP BY u.customerid, r.\"name\"\n" +
                ")\n" +
                "SELECT customer_id, product_name, purchased_count\n" +
                "FROM \"query\" WHERE product_name = '%s' AND purchased_count >= %d", productName, boughtTimes);
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            while (resultSet.next()) {
                customers.add(getCustomerById(resultSet.getInt("customer_id")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(String.format("Ошибка при получении списка покупателей, купивших %s %d раз(-а)", productName, boughtTimes));
        }

        return customers;
    }

    public ArrayList<Customer> getCustomersWithAverageExpensesBetween(int minExpenses, int maxExpenses) throws RuntimeException {
        String sql = String.format("WITH \"query\" as (\n" +
                "SELECT s.id as customer_id, sum(r.price) as expenses\n" +
                "FROM customers s\n" +
                "JOIN purchases u ON s.id = u.customerid\n" +
                "JOIN products r ON r.id = u.productid\n" +
                "GROUP BY s.id ORDER BY s.id\n" +
                ")\n" +
                "SELECT customer_id, expenses FROM \"query\" WHERE expenses > %d AND expenses < %d", minExpenses, maxExpenses);
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            while (resultSet.next()) {
                customers.add(getCustomerById(resultSet.getInt("customer_id")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при получении покупателей по интервалу стоимости");
        }

        return customers;
    }

    public ArrayList<Customer> getBadCustomers(int count) throws RuntimeException {
        String sql = String.format("SELECT s.id as customer_id, count(s.id) as purchases\n" +
                "FROM customers s\n" +
                "JOIN purchases u ON s.id = u.customerid\n" +
                "JOIN products r ON r.id = u.productid\n" +
                "GROUP BY s.id ORDER BY purchases, customer_id\n" +
                "FETCH FIRST %d ROWS ONLY", count);
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            while (resultSet.next()) {
                customers.add(getCustomerById(resultSet.getInt("customer_id")));
            }
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при получении пассивных покупателей");
        }

        return customers;
    }

    public ArrayList<Pair<Customer, ArrayList<ProductSummary>>> getStatsByPeriod(String from, String to) throws RuntimeException {
        String sql = String.format("SELECT purchases.customerid, purchases.productid, sum(products.price) as summary\n" +
                "FROM purchases\n" +
                "JOIN products ON products.id = purchases.productid\n" +
                "WHERE purchasedate >= '%s' AND purchasedate <= '%s'\n" +
                "GROUP BY purchases.customerid, purchases.productid\n" +
                "ORDER BY customerid, productid", from, to);
        ArrayList<Pair<Customer, ArrayList<ProductSummary>>> data = new ArrayList<>();

        try {
            ResultSet resultSet = connection.createStatement().executeQuery(sql);

            Customer customer = null;
            ArrayList<ProductSummary> products = new ArrayList<>();

            while (resultSet.next()) {
                int customerId = resultSet.getInt("customerid");

                if (customer == null) customer = getCustomerById(customerId);
                if (customer.getId() != customerId) {
                    data.add(new Pair<>(customer, products));

                    customer = getCustomerById(customerId);
                    products = new ArrayList<>();
                }

                products.add(new ProductSummary(getProductById(resultSet.getInt("productid")), resultSet.getInt("summary")));
            }
            if (customer != null && products.size() > 0) data.add(new Pair<>(customer, products));
        } catch (SQLException e) {
            throw new RuntimeException("Ошибка при получении статистики за период");
        }

        return data;
    }
}
