package model;

public class Purchase {
    private int id;
    private int customerId;
    private int productId;
    private String purchaseDate;

    public Purchase(int id, int customerId, int productId, String purchaseDate) {
        this.id = id;
        this.customerId = customerId;
        this.productId = productId;
        this.purchaseDate = purchaseDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
}
