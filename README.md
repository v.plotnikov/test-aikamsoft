# Тестовое задание в компанию Aikamsoft

Тестовое задание по работе с PostgreSQL, Java 1.8 и Maven

## Сборка

Чтобы произвести сборку проекта, необходимо клонировать репозиторий с помощью команды:

```bash
git clone <url> aikamsoft -b main
```

Перейти в каталог проекта:

```bash
cd aikamsoft
```

И выполнить сборку с помощью Maven:

```bash
mvn package
```

## Запуск

Импортируйте базу данных через pgAdmin (дамп БД был создан в Windows, поэтому при работе с Linux могут возникнуть
проблемы с кодировкой). Используйте для этого файл *.sql* (plain) или *.psql* (custom). Также приложение ожидает, что
имя базы будет *aikamsoft*, имя пользователя *postgres*, а пароль — *admin*. Конечная точка — *jdbc:postgresql://127.0.0.1:5432/aikamsoft*.

Выполните команду для запуска:

```bash
java -jar target/aikamsoft-1.0.jar search input.json output.json
```
